
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class ControlProduct {
    private static ArrayList<Product> productList = new ArrayList<>();
    
    static{
        load();       
    }
    
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }

    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }
    
    public static boolean updateProduct(int index,Product porduct) {
        productList.set(index, porduct);
        save();
        return true;
    }
    
    public static Product getProduct(int index) {
        return productList.get(index);
       
    }
    
    public static ArrayList<Product> getProductList() {
        return productList;
    }
    
    public static String totalPrice(){
        double total = 0;
        for(int i=0;i<productList.size();i++){
            total += productList.get(i).getPrice() 
                     * productList.get(i).getAmount();
        }
        return total+"";
    }
    
    public static String totalAmount(){
        int total = 0;
        for(int i=0;i<productList.size();i++){
            total += productList.get(i).getAmount();
        }
        return total+"";
    }
    
    public static void runID(){
        for(int i=0;i<productList.size();i++){
            productList.get(i).setId(i+1);
        }
    }
    
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("sumo.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        }catch (FileNotFoundException ex){
          Logger.getLogger(ControlProduct.class.getName()).log(Level.SEVERE, null, ex);
        }catch(IOException ex){
          Logger.getLogger(ControlProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("sumo.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        }catch (FileNotFoundException ex){
            Logger.getLogger(ControlProduct.class.getName()).log(Level.SEVERE, null, ex);
        }catch(IOException ex){
            Logger.getLogger(ControlProduct.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ControlProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
